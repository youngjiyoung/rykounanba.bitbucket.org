function loadPage(wichPage){

     var content = document.getElementById("content");
     

     var ajaxLoad = function(_url){
              $("#content").load(_url ,function(){
                   $("#content").removeClass("slideIn");
                    $("#content").addClass("slideIn");
                   });
     }

     switch(wichPage){
          case "home":
               ajaxLoad("home-content.html");
          break;

          case "showcase":
               ajaxLoad("showcase-content.html");
          break;

          case "blog":
               ajaxLoad("blog-content.html");
          break;

          case "about":
               ajaxLoad("about-content.html");
                break;
     }

}
