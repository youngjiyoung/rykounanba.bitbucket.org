function changeSubMenu(x){
     
     var submenu = document.getElementById("submenu");
     
     while(submenu.firstChild){
          submenu.removeChild(submenu.firstChild);
     }

     var createLi = function(){
          var test = document.createElement('li');
          test.className = "slideIn";
          return test;
     }
     
     var createA = function(link, name){
          var a = document.createElement('a');
          a.href = link;

          var h3 = document.createElement('h2');
          h3.innerHTML = name;
          
          a.appendChild(h3);
          a.target = "_blank";

          return a;
     }

     var changeSelected = function(x){

              var menu = document.getElementById("menu").getElementsByTagName("li");
              var selected = menu[x];
               
              for(var i = 0; i < menu.length; i++){
                    menu[i].removeAttribute("class");
              } 
              
              selected.className="selectedMenu";
     } 

     switch(x){
          case "Reddit":
               //do reddit shit
               var holder = createLi();
               holder.appendChild(createA("http://reddit.com", "core"));
               submenu.appendChild(holder);

               var holder2 = createLi();
               holder2.appendChild(createA("http://reddit.com/r/codcompetitive", "r/CodCompetitive"));
               submenu.appendChild(holder2);

               var holder3 = createLi();
               holder3.appendChild(createA("http://reddit.com/r/awesomenauts", "r/Awesomenauts"));
               submenu.appendChild(holder3);
               
               changeSelected(0);
               break;

          case "Music":
               //do the music shit
               var holder = createLi();
               holder.appendChild(createA("http://last.fm", "last.fm"));
               submenu.appendChild(holder);

               changeSelected(1);
               break;

          case "Programming":
               //do the programming shit
               var holder = createLi();
               holder.appendChild(createA("http://gamedev.net", "gamedev.net"));
               submenu.appendChild(holder);

               changeSelected(2);
               break;

           case "Games":
               changeSelected(3);
               break;
     }
}

function completeSearch(){
     var value = document.getElementById("searchValue").value;
     window.open("https://google.com/#q=" + value);
}
