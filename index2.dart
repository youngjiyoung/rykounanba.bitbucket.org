import 'dart:html';

void main(){
    List<Element> menuItems = querySelector("#menu").querySelectorAll("li");

    for(var item in menuItems){
        item.onClick.listen(updateContent);
    }
}

void updateContent(MouseEvent e){
    switch((e.target as Element).innerHtml.toString()){
        case "Home":
            loadPage("home-content.html");
            break;
        
        case "Showcase":
            loadPage("showcase-content.html");
            break;

        case "Blog":
            loadPage("blog-content.html");
            break;
        
        case "About":
            loadPage("about-content.html");
            break;

        default:
            print("defaulted");
            break;
    }
}

void loadPage(String uri){
    print("loading");
    final NodeValidatorBuilder _htmlvalidator = new NodeValidatorBuilder.common()
        ..allowHtml5()
        ..allowElement('a', attributes: ['href']);
    HttpRequest.getString(uri).then((data) => querySelector("#content").setInnerHtml(data, validator: _htmlvalidator ));
}
